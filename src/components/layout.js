/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.com/docs/how-to/querying-data/use-static-query/
 */

import * as React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Header from "./header"
import { ThemeProvider } from './themeContext'
import Bytemath from './../images/ByteMath.svg'
import  Background  from './background';
import { Link } from "gatsby";


const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  return (
    <ThemeProvider>
      <div className="flex flex-col min-h-screen">
        <Header  siteTitle={data.site.siteMetadata?.title || `Title`} class="z-30 fixed flex items-center w-full h-24 sm:h-32">
          <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.7.0/flowbite.min.css" rel="stylesheet" />
        </Header>
        
        <Background> 
          {children}
        </Background>
      </div>
      
        <footer class="p-4 text-sm bg-[#173494] text-white sm:text-base dark:text-white dark:bg-[#8b8b8b]">
          <div class="container">
            <div>
              <div class="social-icons">
                <ul>
                  <li>
                    <a href="mailto: contact@lorenzozambelli.it?Subject=Hello" target="_top" title="Email Lorenzo Zambelli">
                      <svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 512 512" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M464 64H48C21.49 64 0 85.49 0 112v288c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V112c0-26.51-21.49-48-48-48zm0 48v40.805c-22.422 18.259-58.168 46.651-134.587 106.49-16.841 13.247-50.201 45.072-73.413 44.701-23.208.375-56.579-31.459-73.413-44.701C106.18 199.465 70.425 171.067 48 152.805V112h416zM48 400V214.398c22.914 18.251 55.409 43.862 104.938 82.646 21.857 17.205 60.134 55.186 103.062 54.955 42.717.231 80.509-37.199 103.053-54.947 49.528-38.783 82.032-64.401 104.947-82.653V400H48z"></path></svg>
                    </a>
                  </li>
                  <li>
                    <a href="https://linkedin.com/in/zambelli-lorenzo" target="_blank" title="Contact Lorenzo Zambelli at Linkedin" rel="noopener noreferrer">
                      <svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 448 512" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M100.28 448H7.4V148.9h92.88zM53.79 108.1C24.09 108.1 0 83.5 0 53.8a53.79 53.79 0 0 1 107.58 0c0 29.7-24.1 54.3-53.79 54.3zM447.9 448h-92.68V302.4c0-34.7-.7-79.2-48.29-79.2-48.29 0-55.69 37.7-55.69 76.7V448h-92.78V148.9h89.08v40.8h1.3c12.4-23.5 42.69-48.3 87.88-48.3 94 0 111.28 61.9 111.28 142.3V448z"></path></svg>
                    </a>
                  </li>
                  <li>
                    <a href="https://instagram.com/lorenzo_zambo" target="_blank" title="Contact  Lorenzo Zambelli at Twitter" rel="noopener noreferrer">
                      <svg stroke="currentColor" fill="currentColor" stroke-width="0" xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 448 512"><path d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z"/></svg>
                    </a>
                  </li>
                  <li>
                    <a href="https://facebook.com/profile.php?id=100009454359495" target="_blank" title="Contact  Lorenzo Zambelli at Facebook" rel="noopener noreferrer">
                      <svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 320 512" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z"></path></svg>
                    </a>
                  </li>
                  <li>
                    <a href="https://bytemath.lorenzozambelli.it" target="_blank" title="Go to ByteMath" rel="noopener noreferrer">
                      <img src={Bytemath} class="h-[1em] w-[1em] object-scale-down fill-current rounded-full" alt=""/>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div>
              <div class="footer-menu">
                <ul>
                  <li>
                    <p>© {new Date().getFullYear()} &middot; Lorenzo Zambelli</p>
                  </li>
                  <li>
                    <a class="inline-flex space-x-2 text-sm text-white sm:text-base dark:text-white" href="https://gitlab.com/b64-Lorenzo/mywebsite">
                      <svg  fill="currentColor" xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 512 512"><path d="M503.5 204.6L502.8 202.8L433.1 21.02C431.7 17.45 429.2 14.43 425.9 12.38C423.5 10.83 420.8 9.865 417.9 9.57C415 9.275 412.2 9.653 409.5 10.68C406.8 11.7 404.4 13.34 402.4 15.46C400.5 17.58 399.1 20.13 398.3 22.9L351.3 166.9H160.8L113.7 22.9C112.9 20.13 111.5 17.59 109.6 15.47C107.6 13.35 105.2 11.72 102.5 10.7C99.86 9.675 96.98 9.295 94.12 9.587C91.26 9.878 88.51 10.83 86.08 12.38C82.84 14.43 80.33 17.45 78.92 21.02L9.267 202.8L8.543 204.6C-1.484 230.8-2.72 259.6 5.023 286.6C12.77 313.5 29.07 337.3 51.47 354.2L51.74 354.4L52.33 354.8L158.3 434.3L210.9 474L242.9 498.2C246.6 500.1 251.2 502.5 255.9 502.5C260.6 502.5 265.2 500.1 268.9 498.2L300.9 474L353.5 434.3L460.2 354.4L460.5 354.1C482.9 337.2 499.2 313.5 506.1 286.6C514.7 259.6 513.5 230.8 503.5 204.6z"/></svg>
                       <p>Source Code</p>
                    </a>
                  </li>
                  <li>
                    <Link
                    to="/privacy-policy"
                    class="text-white rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-[#173494] md:p-0 dark:text-white md:dark:hover:text-[#173494] dark:hover:bg-[#173494] dark:hover:text-[#173494] md:dark:hover:bg-transparent dark:border-gray-700"
                    >
                    Privacy Policy
                    </Link>
                  </li>
                  <li>
                    <Link
                    to="/disclaimer"
                    class="block px-6 py-2 pl-3 pr-4 text-white rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-[#173494] md:p-0 dark:text-white md:dark:hover:text-[#173494] dark:hover:bg-[#173494] dark:hover:text-[#173494] md:dark:hover:bg-transparent dark:border-gray-700"
                    >
                    Disclaimer
                    </Link>
                </li>
                </ul>
              </div>
            </div>
          </div>
        </footer>
      
    </ThemeProvider>
    
  )
}

export default Layout
