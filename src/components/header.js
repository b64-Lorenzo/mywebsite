import * as React from "react"
import Toggle from './themeToggle';

const Header = ({ siteTitle }) => (
    <>
        <script
            key="../nodes/flowbite/dist/flowbite.min.js"
            src="../nodes/flowbite/dist/flowbite.min.js"
        />
        <header class="z-30 flex items-center w-full h-24 sm:h-32">
            <div class="container flex items-center justify-between px-6 mx-auto">
                <div class="text-3xl font-black text-gray-800 uppercase dark:text-white">
                    <a href="/">Lorenzo Zambelli</a>
                </div>
            
                <Toggle />
            </div>
        </header>
    </>
)

export default Header
