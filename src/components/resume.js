
import * as React from 'react'
import Rug from './../images/Rug-logo.jpeg'
import Maintenance from "./../components/maintenance";
import Background from './background';

function Resume() {
    return (
        <>
        <div class="p-7 block-section bg-white rounded-xl">
            <h2 class="block-title">Education</h2>
                <div class="mb-5 item-section">
                    <div class="flex-shrink-0 rounded-xl bg-cover">
                        <img src={Rug} class="h-[4em]"/>
                    </div>

                    <div class="w-full space-y-5">
                        <div class="item-header items-end">
                            <div class="space-y-1.5">
                                <div class="font-medium">Master in Applied Mathematics: Computational Mathematics</div>
                                
                                <div class="flex space-x-5">
                                        <div class="item-header-info">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path d="M12 14l9-5-9-5-9 5 9 5z"></path>
                                            <path d="M12 14l6.16-3.422a12.083 12.083 0 01.665 6.479A11.952 11.952 0 0012 20.055a11.952 11.952 0 00-6.824-2.998 12.078 12.078 0 01.665-6.479L12 14z"></path>
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 14l9-5-9-5-9 5 9 5zm0 0l6.16-3.422a12.083 12.083 0 01.665 6.479A11.952 11.952 0 0012 20.055a11.952 11.952 0 00-6.824-2.998 12.078 12.078 0 01.665-6.479L12 14zm-4 6v-7.5l4-2.222"></path>
                                            </svg>
                                            <span>University of Groningen</span>
                                        </div>
                                        <div class="item-header-info">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z"></path>
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 11a3 3 0 11-6 0 3 3 0 016 0z"></path>
                                            </svg>
                                            <span>Groningen</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="space-y-1.5 text-right">
                                    <div class="item-header-info">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"></path>
                                        </svg>
                                        <span>November 2023 – in progress </span>
                                    </div>
                                </div>
                            </div>
                            <div class="border-b border-gray-200"></div>
                        </div>
                    </div>
                    

                    <div class="mb-5 item-section">
                    <div class="flex-shrink-0 rounded-xl bg-cover">
                        <img src={Rug} class="h-[4em]"/>
                    </div>

                    <div class="w-full space-y-5">
                        <div class="item-header items-end">
                            <div class="space-y-1.5">
                                <div class="font-medium">Bachelor in Applied Mathematics</div>
                                
                                <div class="flex space-x-5">
                                        <div class="item-header-info">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path d="M12 14l9-5-9-5-9 5 9 5z"></path>
                                            <path d="M12 14l6.16-3.422a12.083 12.083 0 01.665 6.479A11.952 11.952 0 0012 20.055a11.952 11.952 0 00-6.824-2.998 12.078 12.078 0 01.665-6.479L12 14z"></path>
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 14l9-5-9-5-9 5 9 5zm0 0l6.16-3.422a12.083 12.083 0 01.665 6.479A11.952 11.952 0 0012 20.055a11.952 11.952 0 00-6.824-2.998 12.078 12.078 0 01.665-6.479L12 14zm-4 6v-7.5l4-2.222"></path>
                                            </svg>
                                            <span>University of Groningen</span>
                                        </div>
                                        <div class="item-header-info">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z"></path>
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 11a3 3 0 11-6 0 3 3 0 016 0z"></path>
                                            </svg>
                                            <span>Groningen</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="space-y-1.5 text-right">
                                    <div class="item-header-info">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"></path>
                                        </svg>
                                        <span>September 2020 – 10th November 2023 </span>
                                    </div>
                                </div>
                            </div>
                            <div class="border-b border-gray-200"></div>
                        </div>
                    </div>

                </div>

        <br />

        <div class="p-7 block-section bg-white rounded-xl">
            <h2 class="block-title">Experience</h2>
            
            <Background >
            <Maintenance />
            </Background>
        </div>

        <br />

        
        </>
    )
}

export default Resume