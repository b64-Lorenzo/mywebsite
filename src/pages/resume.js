import * as React from "react"
import Layout from "../components/layout"
import Seo from "../components/seo"
import Breadcrumb from "./../components/breadcrumb";
import Maintenance from "./../components/maintenance";
import Me from './../images/me.png'
import Bg from './../images/bg.png'
import CV from './../../static/CV.pdf'
import Menu from "../components/blog-resume-menu";
import About from "../components/about";

const pages = [
  {title: 'Resume', link : 'resume'}
];

const ResumedPage = () => (
  <Layout>
    <Breadcrumb pages={pages} />
    <br />
    <div class="grid gap-5 lg:grid-cols-3">

            <div class="space-y-5">{/* Start Left Side */}

                <div class="shadow rounded-xl overflow-hidden">{/* Start User Block */}
                    <div class="h-32 bg-cover">
                      <img src={Bg} alt="background"/>
                    </div>
                    <div class="pt-14 p-7 bg-white relative">
                        <span class="status-badge bg-[#ADFC44]">Free</span>
                        <img src={Me} alt="Me" class="user-photo"/>
                        <div class="text-lg font-semibold mb-1.5">Lorenzo Zambelli, BSc</div>
                        <div class="text-sm text-gray-400 mb-7">Master student in Applied Mathematics</div>
                        <div class="flex group">
                          <button class="download-btn"><a href={CV} download>Download CV</a></button>
                          <button class="download-btn-icon"><a href={CV} download><svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.8" d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-4l-4 4m0 0l-4-4m4 4V4"></path></svg></a></button>
                          
                        </div>
                    </div>
                </div>{/* End User Block */}

                <div class="p-7 block-section bg-white rounded-xl">{/* Start Info Block */}
                    <h2 class="block-title">Information</h2>
                    <div class="space-y-4">
                        <div class="flex justify-between">
                            <div class="text-gray-400">Location</div>
                            <div class="font-medium text-right text-gray-600">Groningen</div>
                        </div>
                        <div class="flex justify-between">
                            <div class="text-gray-400">Origin</div>
                            <div class="font-medium text-right text-gray-600">Italian</div>
                        </div>
                        <div class="flex justify-between">
                            <div class="text-gray-400">Relocation</div>
                            <div class="font-medium text-right text-gray-600">Yes</div>
                        </div>
                    </div>
                </div>{/* End Info Block */}

                <div class="p-7 block-section flow-root bg-white rounded-xl">{/* Start Skills Block */}
                    <h2 class="block-title">Skills</h2>
                    <span class="inline-block mb-4 text-base text-teal-500 uppercase font-semibold tracking-[.2rem]">
                        Programming
                    </span>

                    <div class="-m-2 flex flex-wrap">
                        <span class="skill-tag">LaTeX</span>
                        <span class="skill-tag">Python</span>
                        <span class="skill-tag">JavaScript/Typescript</span>
                        <span class="skill-tag">React</span>
                        <span class="skill-tag">HTML/CSS</span>
                        <span class="skill-tag">R</span>
                    </div>
                    <br />
                    <br />
                    <span class="block mb-4 text-base text-teal-500 uppercase font-semibold tracking-[.2rem]">
                        Operating Systems
                    </span>
                    <span class="skill-tag">MacOS</span>
                    <span class="skill-tag">Windows</span>
                    <br />
                    <br />
                    <span class="block mb-4 text-base text-teal-500 uppercase font-semibold tracking-[.2rem]">
                        Software & Tools
                    </span>
                    <span class="skill-tag">Office</span>
                    <span class="skill-tag">STATA</span>
                    <span class="skill-tag">Matlab</span>
                    <br />
                    <br />
                    <span class="block mb-4 text-base text-teal-500 uppercase font-semibold tracking-[.2rem]">
                        Soft skills
                    </span>
                    <span class="skill-tag">Leadership</span>
                    <span class="skill-tag">Interpersonal Skills</span>
                    <span class="skill-tag">Take responsibility</span>
                    <span class="skill-tag">Problem solving</span>
                    <span class="skill-tag">Time Management</span>
                    
                    <br />
                    <br />
                    <span class="block mb-4 text-base text-teal-500 uppercase font-semibold tracking-[.2rem]">
                        Languages
                    </span>
                    <span class="skill-tag">Italian</span>
                    <span class="skill-tag">English</span>

                </div>{/* End Skills Block */}

            </div>{/* End Left Side */}
            
            <div class="space-y-5 lg:col-span-2">{/* Start Right Side */}
                <About />

                {/*<div class="border-t border-gray-200 my-5"></div>*/}
                    
                <Menu />

            </div>{/* End Right Side */}

        </div>
        <br />
  </Layout>
)

export const Head = () => <Seo title="Resume" />

export default ResumedPage
