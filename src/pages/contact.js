import * as React from "react"

import Layout from "../components/layout"
import Seo from "../components/seo"
import Breadcrumb from "./../components/breadcrumb";

const pages = [
  {title: 'Contact', link : 'contact'}
];

const ContactPage = () => (
  <Layout>
    <Breadcrumb pages={pages} />
    <div class="py-8 px-4 mx-auto max-w-screen-xl sm:py-16 lg:px-6">
      <div class="max-w-screen-md mb-8 lg:mb-18">
          <h2 class="mb-4 text-4xl tracking-tight font-extrabold text-gray-900 dark:text-white">Contact Me</h2>
          <p class="text-gray-500 sm:text-xl dark:text-gray-400">If you want to contact me you can do it through my email or my social media pages</p>
      </div>
      <div class="space-y-8 md:grid md:grid-cols-2 lg:grid-cols-4 md:gap-12 md:space-y-0">
          <div>
            <a href="mailto: contact@lorenzozambelli.it?Subject=Hello" target="_top" title="Email Lorenzo Zambelli">
              <div class="flex justify-center items-center mb-4 w-10 h-10 rounded-full dark:bg-white lg:h-12 lg:w-12 dark:bg-primary-900">
                <svg class="w-5 h-5 text-primary-600 lg:w-6 lg:h-6 dark:text-primary-300" stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 512 512" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M464 64H48C21.49 64 0 85.49 0 112v288c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V112c0-26.51-21.49-48-48-48zm0 48v40.805c-22.422 18.259-58.168 46.651-134.587 106.49-16.841 13.247-50.201 45.072-73.413 44.701-23.208.375-56.579-31.459-73.413-44.701C106.18 199.465 70.425 171.067 48 152.805V112h416zM48 400V214.398c22.914 18.251 55.409 43.862 104.938 82.646 21.857 17.205 60.134 55.186 103.062 54.955 42.717.231 80.509-37.199 103.053-54.947 49.528-38.783 82.032-64.401 104.947-82.653V400H48z"></path></svg>
              </div>
              <h3 class="mb-2 text-xl font-bold dark:text-white">Email</h3>
            </a>
          </div>
          <div>
            <a href="https://linkedin.com/in/zambelli-lorenzo" target="_blank" title="Contact Lorenzo Zambelli at Linkedin" rel="noopener noreferrer">   
              <div class="flex justify-center items-center mb-4 w-10 h-10 rounded-full dark:bg-white lg:h-12 lg:w-12 dark:bg-primary-900">
                <svg class="w-5 h-5 text-primary-600 lg:w-6 lg:h-6 dark:text-primary-300" stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 448 512" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M100.28 448H7.4V148.9h92.88zM53.79 108.1C24.09 108.1 0 83.5 0 53.8a53.79 53.79 0 0 1 107.58 0c0 29.7-24.1 54.3-53.79 54.3zM447.9 448h-92.68V302.4c0-34.7-.7-79.2-48.29-79.2-48.29 0-55.69 37.7-55.69 76.7V448h-92.78V148.9h89.08v40.8h1.3c12.4-23.5 42.69-48.3 87.88-48.3 94 0 111.28 61.9 111.28 142.3V448z"></path></svg>
              </div>
              <h3 class="mb-2 text-xl font-bold dark:text-white">Linkedin</h3>
            </a>
          </div>
          <div>
            <a href="https://instagram.com/lorenzo_zambo" target="_blank" title="Contact  Lorenzo Zambelli at Twitter" rel="noopener noreferrer">    
              <div class="flex justify-center items-center mb-4 w-10 h-10 rounded-full dark:bg-white lg:h-12 lg:w-12 dark:bg-primary-900">
                <svg class="w-5 h-5 text-primary-600 lg:w-6 lg:h-6 dark:text-primary-300" stroke="currentColor" fill="currentColor" stroke-width="0" xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 448 512"><path d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z"/></svg>
              </div>
              <h3 class="mb-2 text-xl font-bold dark:text-white">Instagram</h3>
            </a>
          </div>
          <div>
            <a href="https://facebook.com/profile.php?id=100009454359495" target="_blank" title="Contact  Lorenzo Zambelli at Facebook" rel="noopener noreferrer">  
              <div class="flex justify-center items-center mb-4 w-10 h-10 rounded-full dark:bg-white lg:h-12 lg:w-12 dark:bg-primary-900">
                <svg class="w-5 h-5 text-primary-600 lg:w-6 lg:h-6 dark:text-primary-300" stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 320 512" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z"></path></svg>
              </div>
              <h3 class="mb-2 text-xl font-bold dark:text-white">Facebook</h3>
            </a>
          </div>
      </div>
  </div>
  </Layout>
)

export const Head = () => <Seo title="Contact" />

export default ContactPage
