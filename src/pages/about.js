import * as React from "react"
import Layout from "../components/layout"
import Seo from "../components/seo"
import Breadcrumb from "./../components/breadcrumb"
import Maintenance from "./../components/maintenance";
import About from "../components/about";

const pages = [
  {title: 'About', link : 'about'}
];

const AboutPage = () => (
  <Layout>
    <Breadcrumb pages={pages} />

    <br />

    <About />

    <br />
  </Layout>
)

export const Head = () => <Seo title="About" />

export default AboutPage
