import * as React from "react"
import Layout from "../components/layout"
import Seo from "../components/seo"


const IndexPage = () => (
  <Layout>
    
  <div class="flex flex-wrap justify-center">

    {/* --- Home item ---- */}
    <div class="hexagon-item flex">
        <div class="hex-item">
          <div class="dark:bg-white"></div>
          <div class="dark:bg-white"></div>
          <div class="dark:bg-white"></div>
        </div>
        <div class="hex-item flex">
          <div class="dark:bg-white"></div>
          <div class="dark:bg-white"></div>
          <div class="dark:bg-white"></div>
        </div>
        <a  class="hex-content" href="welcome">
          <span class="hex-content-inner">
            <span class="title">Welcome</span>
          </span>
          <svg viewBox="0 0 173.20508075688772 200" height="200" width="174" version="1.1" xmlns="http://www.w3.org/2000/svg"><path d="M86.60254037844386 0L173.20508075688772 50L173.20508075688772 150L86.60254037844386 200L0 150L0 50Z" fill="#1e2530"></path></svg>
        </a>
    </div>

    {/* --- About item --- */}
    <div class="hexagon-item flex">
      <div class="hex-item">
        <div class="dark:bg-white"></div>
        <div class="dark:bg-white"></div>
        <div class="dark:bg-white"></div>
      </div>
      <div class="hex-item flex">
        <div class="dark:bg-white"></div>
        <div class="dark:bg-white"></div>
        <div class="dark:bg-white"></div>
      </div>
      <a  class="hex-content" href="about">
        <span class="hex-content-inner">
          <span class="icon">
            <i class="fa fa-bullseye"></i>
          </span>
          <span class="title">About</span>
        </span>
        <svg viewBox="0 0 173.20508075688772 200" height="200" width="174" version="1.1" xmlns="http://www.w3.org/2000/svg"><path d="M86.60254037844386 0L173.20508075688772 50L173.20508075688772 150L86.60254037844386 200L0 150L0 50Z" fill="#1e2530"></path></svg>
      </a>
    </div>

    {/* -- Resume item --  */}
    <div class="hexagon-item flex">
      <div class="hex-item">
        <div class="dark:bg-white"></div>
        <div class="dark:bg-white"></div>
        <div class="dark:bg-white"></div>
      </div>
      <div class="hex-item flex">
        <div class="dark:bg-white"></div>
        <div class="dark:bg-white"></div>
        <div class="dark:bg-white"></div>
      </div>
      <a  class="hex-content" href="resume">
        <span class="hex-content-inner">
          <span class="icon">
            <i class="fa fa-braille"></i>
          </span>
          <span class="title">Resume</span>
        </span>
        <svg viewBox="0 0 173.20508075688772 200" height="200" width="174" version="1.1" xmlns="http://www.w3.org/2000/svg"><path d="M86.60254037844386 0L173.20508075688772 50L173.20508075688772 150L86.60254037844386 200L0 150L0 50Z" fill="#1e2530"></path></svg>
      </a>    
    </div>
  </div>

  {/* --- Second line --- */}
  <div class="flex-wrap lg:place-content-center flex justify-center">

    {/* -- Works item -- */}
    <div class="hexagon-item flex">
      <div class="hex-item">
      <div class="dark:bg-white"></div>
      <div class="dark:bg-white"></div>
      <div class="dark:bg-white"></div>
    </div>
    <div class="hex-item flex">
      <div class="dark:bg-white"></div>
      <div class="dark:bg-white"></div>
      <div class="dark:bg-white"></div>
    </div>
    <a  class="hex-content" href="works">
      <span class="hex-content-inner">
        <span class="icon">
          <i class="fa fa-id-badge"></i>
        </span>
        <span class="title">Works</span>
      </span>
      <svg viewBox="0 0 173.20508075688772 200" height="200" width="174" version="1.1" xmlns="http://www.w3.org/2000/svg"><path d="M86.60254037844386 0L173.20508075688772 50L173.20508075688772 150L86.60254037844386 200L0 150L0 50Z" fill="#1e2530"></path></svg>
    </a>
  </div>

  {/* --- Contact item --  */}
    <div class="hexagon-item flex">
      <div class="hex-item">
        <div class="dark:bg-white"></div>
        <div class="dark:bg-white"></div>
        <div class="dark:bg-white"></div>
      </div>
      <div class="hex-item flex">
        <div class="dark:bg-white"></div>
        <div class="dark:bg-white"></div>
        <div class="dark:bg-white"></div>
      </div>
          
      <a  class="hex-content" href="contact">
        <span class="hex-content-inner">
          <span class="icon">
            <i class="fa fa-clipboard"></i>
          </span>
          <span class="title">Contact</span>
        </span>
        <svg viewBox="0 0 173.20508075688772 200" height="200" width="174" version="1.1" xmlns="http://www.w3.org/2000/svg"><path d="M86.60254037844386 0L173.20508075688772 50L173.20508075688772 150L86.60254037844386 200L0 150L0 50Z" fill="#1e2530"></path></svg>
      </a>
    </div> 
  </div>
</Layout>
)

export const Head = () => <Seo title="Home" />

export default IndexPage

