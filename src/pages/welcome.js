import * as React from "react"
import Layout from "../components/layout"
import Seo from "../components/seo"
import Breadcrumb from "./../components/breadcrumb";
import Maintenance from "./../components/maintenance";

const pages = [
  {title: 'Welcome', link : 'welcome'}
];


const WelcomePage = () => (
  <Layout>
    <Breadcrumb pages={pages} />
    <br />
    <section class="py-24 bg-trasnparent mx-auto max-w-5xl">
      <div class="container px-4 mx-auto">
        <div class="mx-auto text-left">
          <h2 class="mb-6 text-3xl md:text-5xl lg:text-7xl text-gray-900 font-bold tracking-tight leading-snug md:leading-snug lg:leading-snug dark:text-white">
            Welcome!
          </h2>
          <p class="text-lg md:text-xl text-gray-500 font-medium leading-loose md:leading-loose">
          This is my personal webpage where you can find information about me and my blog: ByteMath!
          </p>
        </div>
      </div>
    </section>

    <br />
  </Layout>
)

export const Head = () => <Seo title="Welcome" />

export default WelcomePage
