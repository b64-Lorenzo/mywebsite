import * as React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Seo from "../components/seo"

const PrivacypolicyPage = () => (
  <Layout>
    <section class="py-24 bg-trasnparent mx-auto max-w-5xl">
      <div class="container px-4 mx-auto">
        <div class="mx-auto text-left">
          <h2 class="mb-6 text-3xl md:text-5xl lg:text-7xl text-gray-900 font-bold tracking-tight leading-snug md:leading-snug lg:leading-snug dark:text-white">
          Privacy Policy
          </h2>
          <br />
          <br />
          <span class="inline-block mb-4 text-base text-teal-500 uppercase font-semibold tracking-[.2rem]">
            General information
          </span>
          <p class="text-lg md:text-xl text-gray-500 font-medium leading-loose md:leading-loose">
            The following notes give a simple overview of what happens to your personal data when you visit this website. Personal data is all data with which you personally can be identified. Detailed information on the topic Data protection can be found in the text below.
          </p>
          <br />
          <br />
          <h3 class="mb-6 text-3xl md:text-5xl lg:text-7xl text-gray-900 font-bold tracking-tight leading-snug md:leading-snug lg:leading-snug dark:text-white">1. Data collection on this website </h3>
          <span class="inline-block mb-4 text-base text-teal-500 uppercase font-semibold tracking-[.2rem]">
            Who is responsible for data collection on this website?
          </span>
          <p class="text-lg md:text-xl text-gray-500 font-medium leading-loose md:leading-loose">
          The data processing on this website is carried out by the website operator. You can find their contact details in the 'Notice to responsible body' in this data protection declaration.</p>
          <br />
          <br />
          <span class="inline-block mb-4 text-base text-teal-500 uppercase font-semibold tracking-[.2rem]">
            How do we collect your data?
          </span>
          <p class="text-lg md:text-xl text-gray-500 font-medium leading-loose md:leading-loose">
            On the one hand, your data is collected when you communicate it to us. This can be e.g. trade data that you put in a contact form.
            <br />
            <br />
            Other data is collected automatically or with your consent when you visit the Website recorded by our IT systems. These are mainly technical data (e.g. internet browser, operating system or time of the page view). This data is collected automatically as soon as you visit this website.
          </p>
          <br />
          <br />
          <span class="inline-block mb-4 text-base text-teal-500 uppercase font-semibold tracking-[.2rem]">
            What do we use your data for?
          </span>
          <p class="text-lg md:text-xl text-gray-500 font-medium leading-loose md:leading-loose">
            Part of the data is collected to ensure error-free provision of the website. Other data may be used to analyze your user behaviour (not recorded at the moment).
          </p>
          <br />
          <br />
          <span class="inline-block mb-4 text-base text-teal-500 uppercase font-semibold tracking-[.2rem]">
            What rights do you have regarding your data?
          </span>
          <p class="text-lg md:text-xl text-gray-500 font-medium leading-loose md:leading-loose">
          You have the right at any time to receive information about the origin, recipients and purpose of your stored personal data. You also have a right to have the rectification or to request the deletion of this data. If you consent to us for data processing, you can revoke this consent at any time for the future. Also, you have the right, under certain circumstances, to request the restriction of the processing of your personal data. Furthermore, you have the right to appeal to the competent supervisory authority.
          <br />
          <br />
          You can contact us about this and other questions on the subject of data protection contact us at any time.
          </p>
          <br />
          <br />
          <span class="inline-block mb-4 text-base text-teal-500 uppercase font-semibold tracking-[.2rem]">
            Analysis Tools and Third Party Tools
          </span>
          <p class="text-lg md:text-xl text-gray-500 font-medium leading-loose md:leading-loose">
            When you visit this website, your surfing behaviour can be statistically evaluated. This is mainly done with so-called analysis programs.
            <br />
            <br />
            Detailed information about these analysis programs can be found in the the following data protection declaration.
          </p>
          <br />
          <br />
          <h3 class="mb-6 text-3xl md:text-5xl lg:text-7xl text-gray-900 font-bold tracking-tight leading-snug md:leading-snug lg:leading-snug dark:text-white">2. Hosting </h3>
          <span class="inline-block mb-4 text-base text-teal-500 uppercase font-semibold tracking-[.2rem]">
            External hosting
          </span>
          <p class="text-lg md:text-xl text-gray-500 font-medium leading-loose md:leading-loose">
          This website is hosted by an external service provider (hoster). The personal data collected on this website will be stored on the hoster's servers. This can be IP addresses, contact requests, meta and communication data, contract data, contact data, names, website hits and other data obtained that is generated via a website.
          <br />
          <br />
          The hoster is used for the purpose of fulfilling the contract to our potential and existing customers (Art. 6 para. 1 lit. b GDPR) and in the interest of a secure, fast and efficient provision of our online offer by a professional provider (Art. 6 para. 1 lit. f GDPR). If a corresponding consent is requested, the processing of data works exclusively on the basis of Art. 6 Paragraph 1 lit. a GDPR and § 25 para. 1 TTDSG, insofar as the consent includes the storage of cookies or access to information in the end device of the user (e.g. device fingerprinting) within the meaning of the TTDSG. The consent can be revoked at any time.
          <br />
          <br />
          Our hoster will only process your data to the extent necessary for fulfillment of his performance obligations is necessary and our instructions in relation to this data.
          </p>
          <br />
          <br />
          <h3 class="mb-6 text-3xl md:text-5xl lg:text-7xl text-gray-900 font-bold tracking-tight leading-snug md:leading-snug lg:leading-snug dark:text-white">3. General information and mandatory information </h3>
          <span class="inline-block mb-4 text-base text-teal-500 uppercase font-semibold tracking-[.2rem]">
            Data protection
          </span>
          <p class="text-lg md:text-xl text-gray-500 font-medium leading-loose md:leading-loose">
          The operators of these pages take the protection of your personal data very seriously. We treat your personal data confidentially and in accordance with the statutory data protection regulations and this Privacy Policy.
          <br />
          <br />
          You can contact us about this and other questions on the subject of data protection contact us at any time.
          <br />
          <br />
          If you use this website, various personal data are raised. Personal data is data with which you personally can be identified. This Privacy Policy explains what data we collect and what we use it for. It also explains how and for what purpose this happens.
          <br />
          <br />
          We would like to point out that data transmission on the internet (e.g.  when communicating by e-mail) may have security gaps. A complete protection of data from access by third parties is not possible.
          </p>
          <br />
          <br />
          <span class="inline-block mb-4 text-base text-teal-500 uppercase font-semibold tracking-[.2rem]">
            What rights do you have regarding your data?
          </span>
          <p class="text-lg md:text-xl text-gray-500 font-medium leading-loose md:leading-loose">
          You have the right at any time to receive information about the origin, recipients and purpose of your stored personal data. You also have a right to have the rectification or to request the deletion of this data. If you consent to us for data processing, you can revoke this consent at any time for the future. Also, you have the right, under certain circumstances, to request the restriction of the processing of your personal data. Furthermore, you have the right to appeal to the competent supervisory authority.
          <br />
          <br />
          You can contact us about this and other questions on the subject of data protection contact us at any time.
          </p>
          <br />
          <br />
          <span class="inline-block mb-4 text-base text-teal-500 uppercase font-semibold tracking-[.2rem]">
            Note on the responsible body
          </span>
          <p class="text-lg md:text-xl text-gray-500 font-medium leading-loose md:leading-loose">
          The responsible body for data processing on this website is:
          <ul>
              <li>
                Lorenzo Zambelli
              </li>
              <li>
                E-mail: contact@lorenzozambelli.it
              </li>
          </ul>
          <br />
          <br />
          The responsible body is the natural or legal person who alone or jointly with others about the purposes and means of Processing of personal data (e.g. names, e-mail addresses, etc.).) decides.
          </p>
          <br />
          <br />
          <span class="inline-block mb-4 text-base text-teal-500 uppercase font-semibold tracking-[.2rem]">
            Storage duration
          </span>
          <p class="text-lg md:text-xl text-gray-500 font-medium leading-loose md:leading-loose">
          As far as no more specific storage period in this data protection declaration was mentioned, your personal data will remain with us, until the purpose for data processing no longer applies. If you assert a legitimate deletion requests or consent to revoke data processing, your data will be deleted if we have no other lawful grounds for storage of your personal data (e.g. tax or commercial law retention periods); in the latter case, the deletion takes place after cessation of these reasons.
          </p>
          <br />
          <br />
          <span class="inline-block mb-4 text-base text-teal-500 uppercase font-semibold tracking-[.2rem]">
            General information on the legal basis for data processing on this website site
          </span>
          <p class="text-lg md:text-xl text-gray-500 font-medium leading-loose md:leading-loose">
          If you have consented to data processing, we will process yours personal data on the basis of Art. 6 Para. 1 lit. a GDPR or Art. 9 Paragraph 2 lit. a GDPR, if special data categories according to Art. 9 Paragraph 1 GDPR are processed. In the case of an express consent for the transfer of personal data to third countries, Data processing is also based on Article 49 (1) lit. a GDPR. If you have consented to the storage of cookies or to the access to information into your end device (e.g. via device fingerprinting), the data processing also takes place on basis of § 25 paragraph 1 TTDSG. Consent is at any time revocable. If your data, for the fulfillment of the contract or for execution of pre-contractual measures, is required, we process your data on the basis of Article 6 (1) (b) GDPR. Further we process your data if it is necessary to fulfill a legal obligation Obligation is required on the basis of Art. 6 Para. 1 lit. c GDPR. The data processing can also be based on our legitimate interests according to Art. 6 Para. 1 lit. f GDPR. About the respective, in individual cases, relevant legal bases, the following paragraphs of this data protection declaration informs.
          </p>
          <br />
          <br />
          <span class="inline-block mb-4 text-base text-teal-500 uppercase font-semibold tracking-[.2rem]">
            Revocation of your consent to data processing
          </span>
          <p class="text-lg md:text-xl text-gray-500 font-medium leading-loose md:leading-loose">
          Many data processing operations are only possible with your expression of consent. You can revoke consent that has already been given at any time. The legality of the data processing that has taken place until the revocation remains unaffected by the revocation.
          </p>
          <br />
          <br />
          <span class="inline-block mb-4 text-base text-teal-500 uppercase font-semibold tracking-[.2rem]">
          Right to lodge a complaint with the competent supervisory authority
          </span>
          <p class="text-lg md:text-xl text-gray-500 font-medium leading-loose md:leading-loose">
          In the event of violations of the GDPR, the data subject has a Right to lodge a complaint with a supervisory authority, in particular in the Member State of their habitual residence, place of work or of the place of the alleged infringement. The right of appeal exists without prejudice to other administrative or judicial legal remedies.
          </p>
          <br />
          <br />
          <span class="inline-block mb-4 text-base text-teal-500 uppercase font-semibold tracking-[.2rem]">
          Right to Data Portability
          </span>
          <p class="text-lg md:text-xl text-gray-500 font-medium leading-loose md:leading-loose">
          You have the right to request data that we collect based on your consent or in Process by automated means for the performance of a contract, in itself or to a deliver to third parties in a commonly used, machine-readable format. If you want the direct transfer of data to another responsible person, this will only be done to the extent that it is technically feasible.
          </p>
          <br />
          <br />
          <span class="inline-block mb-4 text-base text-teal-500 uppercase font-semibold tracking-[.2rem]">
          SSL or TLS encryption
          </span>
          <p class="text-lg md:text-xl text-gray-500 font-medium leading-loose md:leading-loose">
          This site uses for security reasons and to protect the Transmission of confidential content, such as orders or requests that you send to us as the site operator, an SSL or TLS encryption. You can recognize an encrypted connection if the browser's address line changes from “http://” to “https://” and the lock icon appears in your browser line.
          <br />
          <br />
          If SSL or TLS encryption is activated, the Data that you transmit to us cannot be read by third parties.
          </p>
          <br />
          <br />
          <span class="inline-block mb-4 text-base text-teal-500 uppercase font-semibold tracking-[.2rem]">
          Access, Deletion and Correction
          </span>
          <p class="text-lg md:text-xl text-gray-500 font-medium leading-loose md:leading-loose">
          You have the right to restrict the processing of your to request personal data. You can do this at any time contact us. The right to restriction of processing consists in following cases:
          <ul>
            <li>
              If you question the accuracy of your personal data stored by us deny, we usually need time to do this Check. You have this for the duration of the exam Right to restrict processing of your personal data to demand.
            </li>
            <li>
              If the processing of your personal data is unlawful happened/is happening, instead of deleting you can use the Request restriction of data processing.
            </li>
            <li>
            When we no longer need your personal information, you can return it however, for the exercise, defense or assertion of legal claims, you have the right instead of Erasure the restriction on the processing of your to request personal data.
            </li>
            <li>
            If you have lodged an objection in accordance with Art. 21 Para. 1 GDPR, a balance between your interests and ours must be made. As long as it is not clear whose interests prevail, you have the right to restrict the processing of your personal data to request data.
            </li>
          </ul>
          If you have restricted the processing of your personal data have, this data – apart from their storage - only with your consent or to assert, exercise or defend a legal claim or protect the rights of a person another natural or legal person or for reasons of a important public interest of the European Union or one Member State are processed.
          </p>
          <br />
          <br />
          <span class="inline-block mb-4 text-base text-teal-500 uppercase font-semibold tracking-[.2rem]">
          Objecting to Promotional Emails
          </span>
          <p class="text-lg md:text-xl text-gray-500 font-medium leading-loose md:leading-loose">
          The use of published within the scope of the imprint obligation Contact details for sending unsolicited emails Advertising and information materials are hereby contradicted. The operators the sides expressly reserve the right to take legal action in the event of unsolicited sending of advertising information, such as spam e-mails.
          </p>
          <br />
          <br />
          <h3 class="mb-6 text-3xl md:text-5xl lg:text-7xl text-gray-900 font-bold tracking-tight leading-snug md:leading-snug lg:leading-snug dark:text-white">4. Data collection on this website </h3>
          <span class="inline-block mb-4 text-base text-teal-500 uppercase font-semibold tracking-[.2rem]">
          Inquiry by e-mail or by any other means
          </span>
          <p class="text-lg md:text-xl text-gray-500 font-medium leading-loose md:leading-loose">
          If you contact us by e-mail, instagram, etc... your request including all resulting personal data (name, request) stored with us for the purpose of processing your request and processed. We do not pass on this data without your consent.
          <br />
          <br />
          This data is processed on the basis of Article 6 Paragraph 1 Letter b GDPR, provided your request is related to the performance of a contract related or to carry out pre-contractual measures is required. In all other cases, the processing is based on our legitimate interest in the effective processing of the data sent to us directed inquiries (Art. 6 Para. 1 lit. f GDPR) or on your consent (Art. 6 Para. 1 lit. a GDPR) if this was requested; the consent is revocable at any time.
          <br />
          <br />
          The data you sent to us via contact requests will remain with you us until you ask us to delete your consent to the Object to storage or purpose for data storage does not apply (e.g. after your request has been processed). Mandatory Legal Provisions - especially legal retention periods - remain untouched.
          </p>
          <br />
          <br />
          <h3 class="mb-6 text-3xl md:text-5xl lg:text-7xl text-gray-900 font-bold tracking-tight leading-snug md:leading-snug lg:leading-snug dark:text-white">5. Plugins and Tools </h3>
          <span class="inline-block mb-4 text-base text-teal-500 uppercase font-semibold tracking-[.2rem]">
          Font Awesome
          </span>
          <p class="text-lg md:text-xl text-gray-500 font-medium leading-loose md:leading-loose">
          This site uses Font Awesome for a uniform display of fonts and symbols. The provider is Fonticons, Inc., 6 Porter Road Apartment 3R, Cambridge, MA, USA.
          <br />
          <br />
          When you call up a page, your browser loads the required fonts their browser cache to display text, fonts and symbols correctly. to For this purpose, the browser you are using must be able to connect to the servers record from Font Awesome. This gives Font Awesome knowledge that this website was accessed via your IP address. Font Awesome is used on the basis of Article 6 Paragraph 1 Letter f GDPR. We have a legitimate interest in the uniform presentation of the typeface on our website. If you have given your consent has been queried, the processing is carried out exclusively on the basis of Art. 6 Para. 1 lit. a GDPR and § 25 para. 1 TTDSG, insofar as the Consent to the storage of cookies or access to information in the End device of the user (e.g. device fingerprinting) within the meaning of TTDSG includes. The consent can be revoked at any time.
          <br />
          <br />
          If your browser does not support Font Awesome, a Default font is used by your computer.
          <br />
          <br />
For more information about Font Awesome, see and the Font Awesome Privacy Policy at: <a href="https://fontawesome.com/privacy" >https://fontawesome.com/privacy</a>.
          </p>
        </div>
      </div>
    </section>
</Layout>
)

export const Head = () => <Seo title="Privacy Policy" />

export default PrivacypolicyPage
