import * as React from "react"
import Layout from "../components/layout"
import Seo from "../components/seo"
import Breadcrumb from "./../components/breadcrumb";
import Maintenance from "./../components/maintenance";

const pages = [
  {title: 'Works', link : 'works'}
];


const WorksPage = () => (
  <Layout>
    <Breadcrumb pages={pages} />
    <Maintenance/>
  </Layout>
)

export const Head = () => <Seo title="Works" />

export default WorksPage
