import * as React from "react"
import Layout from "../components/layout"
import Seo from "../components/seo"

const DisclaimerPage = () => (
  <Layout>
    <section class="py-24 bg-trasnparent mx-auto max-w-5xl">
      <div class="container px-4 mx-auto">
        <div class="mx-auto text-left">
          <h2 class="mb-6 text-3xl md:text-5xl lg:text-7xl text-gray-900 font-bold tracking-tight leading-snug md:leading-snug lg:leading-snug dark:text-white">
            Disclaimer
          </h2>
          <br />
          <br />
          <span class="inline-block mb-4 text-base text-teal-500 uppercase font-semibold tracking-[.2rem]">
          Legal Notice (Disclaimer)
          </span>
          <p class="text-lg md:text-xl text-gray-500 font-medium leading-loose md:leading-loose">
            Please note the important legal information on the content and availability of this website, copyright and external links.
          </p>
          <br />
          <br />
          <span class="inline-block mb-4 text-base text-teal-500 uppercase font-semibold tracking-[.2rem]">
            Content of this website
          </span>
          <p class="text-lg md:text-xl text-gray-500 font-medium leading-loose md:leading-loose">
            The contents of this website are created with utmost care. However, the person named above assumes no liability for the correctness, completeness and topicality of the content provided.
          </p>
          <br />
          <br />
          <span class="inline-block mb-4 text-base text-teal-500 uppercase font-semibold tracking-[.2rem]">
            Site availability
          </span>
          <p class="text-lg md:text-xl text-gray-500 font-medium leading-loose md:leading-loose">
            The person named above will endeavour to offer the service as uninterrupted as possible for retrieval. Even with all due diligence, however, downtimes cannot be ruled out. The person named above reserves the right to change or discontinue its offer at any time. We cannot assume any liability for interruptions or other disruptions caused by files that are not created correctly or formats that are not structured correctly.
          </p>
          <br />
          <br />
          <span class="inline-block mb-4 text-base text-teal-500 uppercase font-semibold tracking-[.2rem]">
            Copyright
          </span>
          <p class="text-lg md:text-xl text-gray-500 font-medium leading-loose md:leading-loose">
          All content and structures of this website are protected by copyright and ancillary copyright. Publication on the World Wide Web or in other Internet services does not constitute a declaration of consent for any other use by third parties. Any use not permitted by Italian copyright law requires the prior written consent of the person named above.
          <br />
          <br />
          We expressly allow and welcome the citing of our documents and the setting of links on our website, as long as it is indicated that the content is from the person named above and this content is not linked to third-party content that conflicts with the interests of the person named above.
          </p>
          <br />
          <br />
          <span class="inline-block mb-4 text-base text-teal-500 uppercase font-semibold tracking-[.2rem]">
            Note on the problem of external links
          </span>
          <p class="text-lg md:text-xl text-gray-500 font-medium leading-loose md:leading-loose">
          The person named above is responsible as content provider for the “own content” that he makes available for use, according to the general laws. Cross-references (“links”) to content provided by other providers are to be distinguished from this own content. Through the cross-reference, the person named above makes “foreign content” available for use, which is marked in this way:
          <br />
          <br />
“Links” are always “living” (dynamic) references. The above-mentioned person checked the third-party content when it was first linked to determine whether it might result in civil or criminal liability. However, he does not constantly check the content to which he refers in his offer for changes that could give rise to new liability. If he discovers or is informed by others that a specific offer to which he has provided a link triggers civil or criminal liability, he will remove the reference to this offer.
          </p>
        </div>
      </div>
    </section>
  </Layout>
)

export const Head = () => <Seo title="Disclaimer" />

export default DisclaimerPage
