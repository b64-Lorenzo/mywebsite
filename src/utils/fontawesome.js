import { library } from '@fortawesome/fontawesome-svg-core'
import { faGitlab } from '@fortawesome/free-solid-svg-icons'
library.add(faGitlab)